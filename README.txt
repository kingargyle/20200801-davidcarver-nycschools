NYC Schools Chase Coding Test Example.

David Carver - 2020-08-02

The following is an implementation of the Chase coding test to display SAT scores for the data
set for the NYC schools.   It displays a list of all the schools with some basic detail information.
Clicking on a School will display additional details including the following:

* SAT average scores for Reading, Writing, and Math.
* The total number of tests.
* The school name for the tests.
* Error handling for the case where a school does not have any data for the SAT scores.

In addition this does make use of an APP Authentication Token, and is added to every network request.

The following technologies are used:

1. Dagger 2 for dependency Injection
2. HILT to simplify the creation of the components and more importantly making it easier to Test code
that is using Dagger2 for depdency injection.  It has advantages and disadvantages when it comes
to usage, but does make life more tolerable when trying to test code that use Dagger2 for
dependency injection.
3. Retrofit, OKHttp for network calls.
4. RXJava2 to make asynchronous calls easier to make network calls off the UI.  (A newer alternative
now being promoted by Google is Coroutines, which can simplify the complexity of setting up the
initial network calls but was out of scope according to the specs for this test).
5. Robolectric for testing Views, Adapters, ViewHolders, Activities, etc.
6. JUnit 4 and AssertJ / AssertJ Android
7. LiveData is used as the main communication point between the Views and the ViewModel.  There are
tests to make sure that RXJava2 is populating the LiveData correctly, and only the ViewModel cann
update the data.  We only expose the immutable LiveData objects, and the ViewModel handles network
requests.
8. EventBus by GreenRobot to simplify click events that are populated in the ViewHolder, so that
a lot of Interfaces for click events aren't passed around.
9. Material Components for easier theming and Material Design implementation.

The ViewModel here can be shared between the two views that use it.  This is done for expediency, and
in general I don't like to share ViewModels and particularly if using MVP presenters amongst multiple
Views.

Building the App

./gradlew clean installDebug

Running the tests:

./gradlew clean testDebugUnitTest

Running the tests in the IDE:

Non Dagger related tests that use Robolectric or JUnit can be run as normal in Android Studio.
If you are going to run Robolectric Tests that use Dependency Injection for fields, there
is a bug with Android Gradle Plugin that causes the tests to fail.   Instead you have to set them
up as Gradle Tests.  For more information on this see:
https://github.com/google/dagger/issues/1956#issuecomment-650336843

App Design

The main goal here was to try and make it as easy as possible to Test the code and the application.
This is why HILT was chosen instead of doing the app in a traditional Dagger 2 methodology.  Dagger
2 in my opinion is not designed for testing as a priority.  Dagger 1 and Toothpick are easier to setup and
create tests around and does not require as many hoops to jump through.   With this said, HILT makes
the setup, injection, and replacement of components much easier, but you really do need to break
down your Modules into smaller chunks.   The smaller you can make the Modules and the more specific
they are, the easier it is to setup a TestModule as part of your Tests to inject mocks in their places.
If you design a couple of large modules, you then have to come up with either a Test module that
extends and replaces just the pieces it needs, or you have to mock everything that was in the module.

By making the modules small, you can just uninstall the modules your test uses, and then you can
create a TestModule that gets installed at the ApplicationComponent level.   This allows just for the
the small pieces to be mocked instead of all the modules.   This Uninstall and Install is necessary
because Dagger 2 does not have the concept of an overrides like Dagger 1 did, and doesn't have an
ObjectGraph that is a Tree graph like ToothPick. Toothpick looks at the Scopes from the bottom on up.
Allowing a Test scope to be added at the bottom of the graph and taking precedence over all the
other scopes.

Part of making the application testable is making sure that when we create a ViewModel and inject
it into the View under test that we get our Mock ViewModel for usage.  Many examples out there
say to override a ViewModelFactory and have it provide your ViewModel.   This adds a bit of
complication in setup of the tests, so instead, the App uses a ViewModelDelegate that is an
Interface that defines methods to retrieve a ViewModel.  An Implementation of this interface
is inject into the View, and it will use the ViewModelProvider under the covers to actually get
the view.  Now for tests, the ViewModelModule is uninstalled, and a Mock of the ViewModelDelegate
is injected in it's place.  This then returns a Mock of the ViewModel, so that we aren't using
the real thing.

All of the above can be seen by looking at the implementation of the MainActivityTest class.

The app's structure breaks down into several main components:

* core - non UI related work and dependency injection modules.
* data - purely data specific items (remote data models, events, etc)
* ui - All android related code, viewmodel, Activies, application.

Material Components tie in the UI, and make styling of the application consistent.

In general I prefer to program to Interfaces, but Google seems to love injecting real classes.
Which is why I use Provides a lot instead of the Constructor Injection definitions.  I also prefer
the finer control instead of just letting the system guess what it should do and hope it is correct.


Additional Notes:

In general the Data Transfer Objects are leaking all the way from the from the back end system. I
prefer a design that includes a Domain Model and maps the data from the DTOs to the Domain Model.
The repositories would only return the Domain Model objects.  This way you changes to the
data source don't affect the UI aspects of the app.  Data can be mapped and massaged to the
best format that the app expects and isn't tied to the source data's structure.

Right now the app has limited handling for times when there is no Network connection.  Ideally it needs
check before making any network request that there is a valid network connection.  It also
needs better indication to the user when something goes wrong.

Google is really starting to push the use of Kotlin and Coroutines over the use of RXJava for
asynchronous communication as of the latest Android 11 developer videos.  I have worked on many
legacy applications and understand that change is slow, and Google also changes its mind at the
drop of a hat.   So what they say now, may change in the future.

The app is 99% written in Java, there are only a few classes that use Kotlin.  There is nothing in
the code that requires Kotlin functionality, but really could leverage some of it.  In particular
requests for initial data could be done Lazily so that request only happens once, and then the
data is cached on each subsequent request.  This can prevent possible LiveData floods of unchanged
data and network requests.

LiveData itself offers some challenges by its nature.  It never clears out the data, and thus,
when the app configuration changes, and the observers are reset it can repopulate the views.  The
issue is that stuff like RecyclerView may also save there own states, if you don't have the DiffUtil
set up correctly, it will appear that the RecyclerView refreshes as a new list is submitted.  The
key here as noted in the ShopDiffUtil is to make sure you have the two comparision methods set up
correctly, so that it doesn't detect any difference.   The other problem is that with large data sets
you can end up maintaining a couple of copies.  One in the the RecyclerView adapter and the same
data in the LiveData holder itself.  Key here would be to implement a PagingAdapter or an
EndlessScrollListener to only load a subset of the data.   This was not done during this test
for expediency purposes.

Jackson was chosen for its familiarity, but Moshi is a good alternative for the JSon data binding
aspects of the application.

Time didn't allow for the setup of Jacoco for unit test reporting, but would be a nice to have to
go along with the Unit Tests to see what is actually being covered.  Most of the critical paths
should be covered, and the SchoolDetailsActivity wasn't tested as examples of Using Robolectric were
already elsewhere in the app.

In general I've tried to do some of the best practices I've developed over the years for designing
and test apps.  Some of these techniques go against established Google architecutre practices, but
I feel having testable code outweighs following any one particular pattern to the letter.  Use
what makes sense, and there is more than one way to accomplish the same thing.  The app doesn't use
Fragments as in general they add another layer of complexity that isn't needed in this simple
application.   They serve their own purposes, but there are other ways to accomplish things.

Just as the code could use some more Individual views that do specific things instead of just
doing all the layouts in the activities.  Again time didn't allow for finer breakdown.

Questions please feel free to reach out to me.  I think I have hit all the major requirements
for the application.

David Carver
d_a_carver@yahoo.com
