package us.nineworlds.chase.nycschools.ui.home;

import org.junit.Before;
import org.junit.Test;

import us.nineworlds.chase.nycschools.data.nyc.model.School;

import static org.assertj.core.api.Assertions.assertThat;

public class SchoolDiffUtilTest {

    private SchoolDiffUtil diffUtil;

    @Before
    public void setUp() {
        diffUtil = new SchoolDiffUtil();
    }

    @Test
    public void areItemsTheSameReturnsTrue() {
        School school1 = new School("1234", "test", "123 somewhere", "somewhere", "OH", "12345");
        School school2 = new School("1234", "test", "123 somewhere", "somewhere", "OH", "12345");

        Boolean result = diffUtil.areItemsTheSame(school1, school2);

        assertThat(result).isTrue();
    }

    @Test
    public void areItemsTheSameReturnsFalse() {
        School school1 = new School("1234", "test", "123 somewhere", "somewhere", "OH", "12345");
        School school2 = new School("14", "test", "123 somewhere", "somewhere", "OH", "12345");

        Boolean result = diffUtil.areItemsTheSame(school1, school2);

        assertThat(result).isFalse();
    }

    @Test
    public void areContentsTheSameReturnsFalse() {
        School school1 = new School("1234", "test", "123 somewhere", "somewhere", "OH", "12345");
        School school2 = new School("14", "test", "123 somewhere", "somewhere", "OH", "12345");

        Boolean result = diffUtil.areContentsTheSame(school1, school2);

        assertThat(result).isFalse();
    }

    @Test
    public void areContentsTheSameReturnsTrue() {
        School school1 = new School("1234", "test", "123 somewhere", "somewhere", "OH", "12345");
        School school2 = new School("1234", "test", "123 somewhere", "somewhere", "OH", "12345");

        Boolean result = diffUtil.areContentsTheSame(school1, school2);

        assertThat(result).isTrue();
    }
}