package us.nineworlds.chase.nycschools.core.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import io.reactivex.Single;
import us.nineworlds.chase.nycschools.core.services.NYCDataSource;
import us.nineworlds.chase.nycschools.data.nyc.model.School;
import us.nineworlds.chase.nycschools.data.nyc.model.SchoolDetail;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.Strict.class)
public class SchoolRepositoryImplTest {

    @Mock
    NYCDataSource mockDataSource;

    @Mock
    School mockSchool;

    @Mock
    SchoolDetail mockSchoolDetail;

    private SchoolRepositoryImpl schoolRepository;

    @Before
    public void setUp() {
        schoolRepository = new SchoolRepositoryImpl(mockDataSource);
    }

    @Test
    public void getSchoolsReturnsExpectedSingle() {
        Single<List<School>> expectedSingle = Single.just(singletonList(mockSchool));
        doReturn(expectedSingle).when(mockDataSource).getAllSchools();

        Single<List<School>> result = schoolRepository.getSchools();

        assertThat(result).isEqualTo(expectedSingle);
        verify(mockDataSource).getAllSchools();
    }

    @Test
    public void getSchoolDetailReturnsExpectedSingleObservable() {
        Single<List<SchoolDetail>> expectedSingle = Single.just(singletonList(mockSchoolDetail));
        String expectedSchoolId = "12327d";
        doReturn(expectedSingle).when(mockDataSource).getSchoolDetail(anyString());

        Single<List<SchoolDetail>> result = schoolRepository.getSchoolDetail(expectedSchoolId);

        assertThat(result).isEqualTo(expectedSingle);
        verify(mockDataSource).getSchoolDetail(expectedSchoolId);
    }

}
