package us.nineworlds.chase.nycschools.ui.home;

import android.view.LayoutInflater;

import androidx.appcompat.view.ContextThemeWrapper;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

import us.nineworlds.chase.nycschools.R;
import us.nineworlds.chase.nycschools.data.events.SchoolCardClickedEvent;
import us.nineworlds.chase.nycschools.data.nyc.model.School;
import us.nineworlds.chase.nycschools.databinding.ItemSchoolBinding;

import static org.assertj.android.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@RunWith(AndroidJUnit4.class)
public class SchoolViewHolderTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

    @Mock
    EventBus mockEventBus;

    private SchoolViewHolder viewHolder;
    private ItemSchoolBinding binding;

    @Before
    public void setUp() {
        ContextThemeWrapper context = new ContextThemeWrapper(ApplicationProvider.getApplicationContext(), R.style.Theme_MyApp);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        binding = ItemSchoolBinding.inflate(layoutInflater);
        viewHolder = new SchoolViewHolder(binding.getRoot());
    }

    @Test
    public void bindSetsExpectedValues() {
        School school = new School("1234", "Test School", "1234 Somewhere", "Somewhere", "CA", "43235");

        viewHolder.bind(school);

        assertThat(binding.itemSchoolPostalcode).hasText("43235");
        assertThat(binding.itemSchoolAddress).hasText("1234 Somewhere");
        assertThat(binding.itemSchoolCity).hasText("Somewhere");
        assertThat(binding.itemSchoolState).hasText("CA");
        assertThat(binding.itemSchoolName).hasText("Test School");
    }

    @Test
    public void clickingOnTheCardSendsExpectedEvent() {
        School school = new School("1234", "Test School", "1234 Somewhere", "Somewhere", "CA", "43235");
        SchoolCardClickedEvent event = new SchoolCardClickedEvent(school.getDbn());
        viewHolder.eventBus = mockEventBus;
        viewHolder.bind(school);

        binding.itemSchoolCard.performClick();

        verify(mockEventBus).post(event);
    }

}