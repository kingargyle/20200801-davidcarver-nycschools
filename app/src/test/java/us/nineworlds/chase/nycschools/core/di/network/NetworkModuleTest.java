package us.nineworlds.chase.nycschools.core.di.network;


import org.junit.Before;
import org.junit.Test;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import us.nineworlds.chase.nycschools.BuildConfig;
import us.nineworlds.chase.nycschools.core.services.NYCDataSource;

import static org.assertj.core.api.Assertions.assertThat;

public class NetworkModuleTest {

    private NetworkModule networkModule;
    private OkHttpClient okHttpClient;

    @Before
    public void setUp() {
        networkModule = new NetworkModule();
        okHttpClient = new OkHttpClient.Builder().build();
    }

    @Test
    public void retrofitProviderIsCreatedAsExpected() {
        HttpUrl expectedUrl = HttpUrl.parse(BuildConfig.NYC_URL_ENDPOINT);

        Retrofit result = networkModule.providesRestClient(okHttpClient);

        assertThat(result.baseUrl()).isNotNull().isEqualTo(expectedUrl);
    }

    @Test
    public void retrofitProviderContainsRxJava2CallAdapterFactory() {
        Retrofit result = networkModule.providesRestClient(okHttpClient);

        assertThat(result.callAdapterFactories()).extracting("class").contains(RxJava2CallAdapterFactory.class);
    }

    @Test
    public void retrofitProviderContainsJacksonConverterFactory() {
        Retrofit result = networkModule.providesRestClient(okHttpClient);

        assertThat(result.converterFactories()).extracting("class").contains(JacksonConverterFactory.class);
    }

    @Test
    public void dataSourceIsCreatedAsExpected() {
        Retrofit retrofit = networkModule.providesRestClient(okHttpClient);

        NYCDataSource result =  networkModule.providesNYCDataSource(retrofit);

        assertThat(result).isNotNull().isInstanceOf(NYCDataSource.class);
    }
}