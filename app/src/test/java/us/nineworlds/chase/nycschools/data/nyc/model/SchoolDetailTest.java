package us.nineworlds.chase.nycschools.data.nyc.model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SchoolDetailTest {

    private static final String SCHOOL_DETAIL_FILE = "/nyctestdata/schooldetail.json";

    private ObjectMapper mapper;

    @Before
    public void setUp() {
        mapper = new ObjectMapper();
    }

    @Test
    public void deserializeSchoolDetail() throws Exception {
        TypeReference<List<SchoolDetail>> listTypeReference = new TypeReference<List<SchoolDetail>>() {
        };

        List<SchoolDetail> schoolDetails = mapper.readValue(this.getClass().getResourceAsStream(SCHOOL_DETAIL_FILE), listTypeReference);

        assertThat(schoolDetails).isNotEmpty();
        assertThat(schoolDetails.get(0).getDbn()).isEqualTo("01M292");
    }

}