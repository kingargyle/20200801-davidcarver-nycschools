package us.nineworlds.chase.nycschools.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.greenrobot.eventbus.EventBus;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;

import java.util.List;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import dagger.hilt.android.testing.HiltAndroidRule;
import dagger.hilt.android.testing.HiltAndroidTest;
import dagger.hilt.android.testing.HiltTestApplication;
import dagger.hilt.android.testing.UninstallModules;
import us.nineworlds.chase.nycschools.core.di.events.EventModule;
import us.nineworlds.chase.nycschools.core.di.viewmodel.ViewModelModule;
import us.nineworlds.chase.nycschools.data.nyc.model.School;
import us.nineworlds.chase.nycschools.ui.viewmodel.SchoolViewModel;
import us.nineworlds.chase.nycschools.ui.viewmodel.ViewModelDelegate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@HiltAndroidTest
@RunWith(AndroidJUnit4.class)
@UninstallModules({EventModule.class, ViewModelModule.class})
@Config(application = HiltTestApplication.class)
public class MainActivityTest {

    @Rule
    public HiltAndroidRule hiltAndroidRule = new HiltAndroidRule(this);

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

    @Mock
    EventBus mockEventBus;

    @Mock
    ViewModelDelegate mockViewModelDelegate;

    @Mock
    SchoolViewModel mockSchoolViewModel;

    @Mock
    LiveData<List<School>> mockLiveData;

    MainActivity activity;

    @After
    public void tearDown() {
        if (activity != null) {
            activity.finish();
        }
    }

    @Test
    public void activityIsCreated() {
        doReturn(mockSchoolViewModel).when(mockViewModelDelegate).findViewModel(any(), any());
        doReturn(mockLiveData).when(mockSchoolViewModel).getAllSchoolsLiveData();

        activity = Robolectric.buildActivity(MainActivity.class).create().get();

        assertThat(activity).isNotNull();
    }

    @Test
    public void viewModelIsRetrievedSuccessfully() {
        doReturn(mockSchoolViewModel).when(mockViewModelDelegate).findViewModel(any(), any());
        doReturn(mockLiveData).when(mockSchoolViewModel).getAllSchoolsLiveData();

        activity = Robolectric.buildActivity(MainActivity.class).create().get();

        verify(mockViewModelDelegate).findViewModel(activity, SchoolViewModel.class);
    }

    @Test
    public void activitySubscribesToTheViewModelLiveDataForAllSchools() {
        doReturn(mockSchoolViewModel).when(mockViewModelDelegate).findViewModel(any(), any());
        doReturn(mockLiveData).when(mockSchoolViewModel).getAllSchoolsLiveData();

        activity = Robolectric.buildActivity(MainActivity.class).create().get();

        verify(mockSchoolViewModel).getAllSchoolsLiveData();
        verify(mockLiveData).observe(eq(activity), any());
    }

    @Test
    public void eventBusIsRegisteredOnResume() {
        doReturn(mockSchoolViewModel).when(mockViewModelDelegate).findViewModel(any(), any());
        doReturn(mockLiveData).when(mockSchoolViewModel).getAllSchoolsLiveData();
        doReturn(false).when(mockEventBus).isRegistered(any());
        activity = Robolectric.buildActivity(MainActivity.class).create().get();

        activity.onResume();

        verify(mockEventBus).isRegistered(activity);
        verify(mockEventBus).register(activity);
    }

    @Module
    @InstallIn(ApplicationComponent.class)
    public class TestModule {

        @Provides
        ViewModelDelegate providesViewModelDelegate() {
            return mockViewModelDelegate;
        }

        @Provides
        EventBus providesEventBus() {
            return mockEventBus;
        }
    }
}