package us.nineworlds.chase.nycschools.core.di.viewmodel;

import org.junit.Before;
import org.junit.Test;

import us.nineworlds.chase.nycschools.ui.viewmodel.ViewModelDelegate;

import static org.assertj.core.api.Assertions.assertThat;

public class ViewModelModuleTest {

    private ViewModelModule module;

    @Before
    public void setUp() {
        module = new ViewModelModule();
    }

    @Test
    public void viewModelDelegateIsCreatedSuccessfully() {
        ViewModelDelegate result = module.providesViewModelDelegate();

        assertThat(result).isNotNull();
    }
}