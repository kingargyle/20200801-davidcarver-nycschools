package us.nineworlds.chase.nycschools.core.di.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import us.nineworlds.chase.nycschools.core.repository.SchoolRepository;
import us.nineworlds.chase.nycschools.core.services.NYCDataSource;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.Strict.class)
public class RepositoryModuleTest {

    @Mock
    NYCDataSource mockDataSource;

    private RepositoryModule module;

    @Before
    public void setUp() {
        module = new RepositoryModule();
    }

    @Test
    public void repostoryIsCreatedSuccesfully() {
        SchoolRepository result = module.providesSchoolRepository(mockDataSource);

        assertThat(result).isNotNull();
    }

}