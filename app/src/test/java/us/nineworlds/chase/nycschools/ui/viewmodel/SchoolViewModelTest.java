package us.nineworlds.chase.nycschools.ui.viewmodel;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.SavedStateHandle;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

import java.util.Collections;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import us.nineworlds.chase.nycschools.core.repository.SchoolRepository;
import us.nineworlds.chase.nycschools.data.nyc.model.School;

import static com.jraska.livedata.TestObserver.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Original this needed Robolectric as the LiveData was executing on the Android UI Thread.
 * However by using the InstantTaskExecutorRule you can avoid using Robolectric and make
 * the ViewModel a pure Unit Test.  Removing the startup overhead of that occurs with Robolectric.
 */
public class SchoolViewModelTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

    /**
     * Needed for working with LiveData values and asserting against them.
     * It prevents the need of having Robolectric to test the ViewModels.
     */
    @Rule
    public InstantTaskExecutorRule taskExecutorRule = new InstantTaskExecutorRule();

    @Mock
    SavedStateHandle mockSavedStateHandle;

    @Mock
    SchoolRepository mockSchoolRepository;

    @Mock
    School mockSchool;

    private Scheduler scheduler = Schedulers.trampoline();
    private SchoolViewModel viewModel;

    @Before
    public void setUp() {
        viewModel = new SchoolViewModel(mockSchoolRepository, scheduler, mockSavedStateHandle);
    }

    @Test
    public void schoolRepositoryUpdatesLiveDataWithExpectedSchoolList() {
        List<School> expectedSchoolList = Collections.singletonList(mockSchool);
        Single<List<School>> expectedObservable = Single.just(expectedSchoolList);
        doReturn(expectedObservable).when(mockSchoolRepository).getSchools();
        TestObserver<List<School>> testSubscriber = new TestObserver<>();
        expectedObservable.subscribe(testSubscriber);

        viewModel.retrieveListOfSchools();

        testSubscriber.assertComplete();
        testSubscriber.assertValue(expectedSchoolList);

        verify(mockSchoolRepository).getSchools();
    }

    /**
     * This sets up the testing that the live data code is executed as expected on the ViewModels
     * subscribe.   It wraps an observer that uses observeForever to receive the values and does
     * the assertions.
     */
    @Test
    public void testExecutionOfSubscribe() throws Exception  {
        // Replace the IO scheduler with a trampoline scheduler to run it immediately
        // We also set at the top the Trampoline for the observeOn this way both
        // are set to the same and should execute immediately
        RxJavaPlugins.setIoSchedulerHandler(scheduler1 -> Schedulers.trampoline());

        List<School> expectedSchoolList = Collections.singletonList(mockSchool);
        Single<List<School>> expectedObservable = Single.just(expectedSchoolList);
        doReturn(expectedObservable).when(mockSchoolRepository).getSchools();

        // Create the test observer to listen for data from the LiveData object
        // This under the covers creates a observeForever()
        // With kotlin we could do an alias here to clean up this code a bit.
        com.jraska.livedata.TestObserver<List<School>> observer = test(viewModel.getAllSchoolsLiveData());

        viewModel.retrieveListOfSchools();

        observer.awaitValue().assertHasValue().assertValue( value -> {
            return expectedSchoolList.equals(value);
        });
    }
}