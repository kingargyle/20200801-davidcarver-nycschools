package us.nineworlds.chase.nycschools.data.nyc.model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SchoolTest {

    private static final String ALL_SCHOOLS_FILE = "/nyctestdata/allschools.json";

    private ObjectMapper mapper;

    @Before
    public void setUp() {
        mapper = new ObjectMapper();
    }

    @Test
    public void deserializeSchool() throws Exception {
        final TypeReference<List<School>> listTypeReference = new TypeReference<List<School>>() {
        };

        final List<School> schoolDetails = mapper.readValue(this.getClass().getResourceAsStream(ALL_SCHOOLS_FILE), listTypeReference);

        assertThat(schoolDetails).isNotEmpty();
        assertThat(schoolDetails.get(0).getDbn()).isEqualTo("02M260");
    }

}