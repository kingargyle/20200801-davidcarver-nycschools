package us.nineworlds.chase.nycschools.ui.home;

import android.content.ContextWrapper;
import android.view.ContextThemeWrapper;
import android.widget.FrameLayout;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

import java.util.Collections;
import java.util.List;

import us.nineworlds.chase.nycschools.R;
import us.nineworlds.chase.nycschools.data.nyc.model.School;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


@RunWith(AndroidJUnit4.class)
public class SchoolListAdapterTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

    @Mock
    SchoolViewHolder mockSchoolViewHolder;

    @Mock
    School mockSchool;

    private SchoolListAdapter adapter;

    @Before
    public void setUp() {
        adapter = new SchoolListAdapter();
    }

    @Test
    public void createViewHolderCreatesExpectedViewHolder() {
        // A context theme wrapper is needed with the Apps theme assigned otherwise view inflation will fail due to missing attributes that are defined in Material themes.
        ContextThemeWrapper context = new ContextThemeWrapper(ApplicationProvider.getApplicationContext(), R.style.Theme_MyApp);
        FrameLayout frameLayout = new FrameLayout(context);

        SchoolViewHolder viewHolder = adapter.onCreateViewHolder(frameLayout, 0);

        assertThat(viewHolder).isNotNull();
    }

    @Test
    public void onBindBindsDataToTheViewHolder() {
        List<School> data = Collections.singletonList(mockSchool);
        adapter.submitList(data);

        adapter.onBindViewHolder(mockSchoolViewHolder, 0);

        verify(mockSchoolViewHolder).bind(mockSchool);
    }
}