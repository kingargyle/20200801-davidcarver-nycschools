package us.nineworlds.chase.nycschools.core.di.rxjava;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Due to needing the Android Looper and Main thread, we need to run this as a Robolectric Test.
 */
@RunWith(AndroidJUnit4.class)
public class RXJavaModuleTest {

    private RXJavaModule rxJavaModule;

    @Before
    public void setUp() {
        rxJavaModule = new RXJavaModule();
    }

    @Test
    public void schedulerIsInstanceOfAndroidMainThread() {
        Scheduler scheduler = rxJavaModule.providesScheduler();

        assertThat(scheduler).isSameAs(AndroidSchedulers.mainThread());
    }
}