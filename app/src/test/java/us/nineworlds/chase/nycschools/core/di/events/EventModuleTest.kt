package us.nineworlds.chase.nycschools.core.di.events

import org.assertj.core.api.Assertions.assertThat
import org.greenrobot.eventbus.EventBus
import org.junit.Before
import org.junit.Test

class EventModuleTest {

    lateinit var module : EventModule

    @Before
    fun setUp() {
        module = EventModule()
    }

    @Test
    fun eventBusIsCreatedSuccessfully() {
        val result = module.providesEventBus();

        assertThat(result).isNotNull.isEqualTo(EventBus.getDefault())
    }
}