package us.nineworlds.chase.nycschools.ui.schooldetail;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;

import com.google.android.material.appbar.MaterialToolbar;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import timber.log.Timber;
import us.nineworlds.chase.nycschools.data.nyc.model.SchoolDetail;
import us.nineworlds.chase.nycschools.databinding.ActivitySchoolDetailsBinding;
import us.nineworlds.chase.nycschools.ui.viewmodel.SchoolViewModel;
import us.nineworlds.chase.nycschools.ui.viewmodel.ViewModelDelegate;

@AndroidEntryPoint
public class SchoolDetailsActivity extends AppCompatActivity {

    @Inject
    ViewModelDelegate viewModelDelegate;

    private SchoolViewModel schoolViewModel;
    private ActivitySchoolDetailsBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivitySchoolDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        schoolViewModel = viewModelDelegate.findViewModel(this, SchoolViewModel.class);

        setupSupportActionBar();
        subscribe();
        lookupSchool();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void lookupSchool() {
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }

        String schoolId = intent.getStringExtra("nyc_school_id");
        if (schoolId != null) {
            Timber.d("School Id: %s", schoolId);
            schoolViewModel.lookupSchoolById(schoolId);
        }
    }

    private void setupSupportActionBar() {
        MaterialToolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);

        ActionBar supportActionBar = getSupportActionBar();
        // Yeah we know it isn't null, but better safe than sorry.
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setTitle("School Details");
        }
    }

    private void subscribe() {
        LiveData<SchoolDetail> schoolDetail = schoolViewModel.getSchoolDetail();
        LiveData<String> errorMessage = schoolViewModel.getErrorMessage();

        schoolDetail.observe(this, this::bindScores);
        errorMessage.observe(this, this::bindErrorMessage);
    }

    private void bindScores(SchoolDetail detail) {
        String mathAvgScore = String.valueOf(detail.getSatMathAvgScore());
        String readingScore = String.valueOf(detail.getAverageReadingScore());
        String writingScore = String.valueOf(detail.getSatWritingAvgScore());
        String schoolname = detail.getSchoolName();
        String testTakersNumber = String.valueOf(detail.getNumberOfSatTestTakers());

        binding.satMathRow.setVisibility(View.VISIBLE);
        binding.satReadingRow.setVisibility(View.VISIBLE);
        binding.satWritingRow.setVisibility(View.VISIBLE);
        binding.satTotalTestsRow.setVisibility(View.VISIBLE);

        binding.satMathScore.setText(mathAvgScore);
        binding.satReadingScore.setText(readingScore);
        binding.satWritingScore.setText(writingScore);
        binding.schoolDetailsTitle.setText(schoolname);
        binding.satTotalTestNumber.setText(testTakersNumber);
    }

    private void bindErrorMessage(String message) {
        if (message.isEmpty()) {
            binding.satErrorMessage.setVisibility(View.GONE);
        } else {
            binding.satMathRow.setVisibility(View.GONE);
            binding.satWritingRow.setVisibility(View.GONE);
            binding.satReadingRow.setVisibility(View.GONE);
            binding.satErrorMessage.setVisibility(View.VISIBLE);
            binding.satErrorMessage.setText(message);
        }
    }
}
