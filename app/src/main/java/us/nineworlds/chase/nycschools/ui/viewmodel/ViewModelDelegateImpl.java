package us.nineworlds.chase.nycschools.ui.viewmodel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

public class ViewModelDelegateImpl implements ViewModelDelegate {

    @Override
    public <A extends AppCompatActivity, V extends ViewModel> V findViewModel(A activity, Class<V> viewModel) {
        return ViewModelProviders.of(activity).get(viewModel);
    }

    @Override
    public <F extends FragmentActivity, VM extends ViewModel> VM findViewModel(F fragment, Class<VM> viewModel) {
        return ViewModelProviders.of(fragment).get(viewModel);
    }
}
