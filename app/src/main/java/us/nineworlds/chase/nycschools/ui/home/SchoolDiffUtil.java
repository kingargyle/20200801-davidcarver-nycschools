package us.nineworlds.chase.nycschools.ui.home;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import java.util.Objects;

import us.nineworlds.chase.nycschools.data.nyc.model.School;

/**
 * If this class isn't setup right (i.e. you reverse the areItemsTheSame and areContentsTheSame
 * logic), the recyclerview will think the content is different all the time and look like it is
 * reloading the data each time and re-animating it.
 */
public class SchoolDiffUtil extends DiffUtil.ItemCallback<School> {

    @Override
    public boolean areItemsTheSame(@NonNull School oldItem, @NonNull School newItem) {
        return Objects.equals(oldItem.getDbn(), newItem.getDbn()) &&
                Objects.equals(oldItem.getSchoolName(), newItem.getSchoolName()) &&
                Objects.equals(oldItem.getPrimaryAddress(), newItem.getPrimaryAddress()) &&
                Objects.equals(oldItem.getCity(), newItem.getCity()) &&
                Objects.equals(oldItem.getState(), newItem.getState()) &&
                Objects.equals(oldItem.getPostalCode(), newItem.getPostalCode());
    }

    @Override
    public boolean areContentsTheSame(@NonNull School oldItem, @NonNull School newItem) {
        return Objects.equals(oldItem, newItem);
    }
}
