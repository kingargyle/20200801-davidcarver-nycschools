package us.nineworlds.chase.nycschools.core.repository;

import androidx.annotation.NonNull;

import java.util.List;

import io.reactivex.Single;
import us.nineworlds.chase.nycschools.data.nyc.model.School;
import us.nineworlds.chase.nycschools.data.nyc.model.SchoolDetail;

/**
 * Note that for expediency we are exposing the Data Transfer objects from the Data Source in this
 * particular interface.  More time, we could setup a Domain Model for the application and then
 * have a mapping layer within the repository implementation that would map from the data transer
 * objects back to the domain, and vice versa.
 */

public interface SchoolRepository {

    @NonNull
    Single<List<School>> getSchools();

    @NonNull
    Single<List<SchoolDetail>> getSchoolDetail(@NonNull String schoolId);
}
