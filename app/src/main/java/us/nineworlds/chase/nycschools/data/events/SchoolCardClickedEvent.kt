package us.nineworlds.chase.nycschools.data.events

/**
 * This doesn't have a unit test as it is primarily a pojo.  The gettters and setters have
 * already been generated by Kotlin along with the toString and hashcodes.   The School and
 * SchoolDetail have tests mainly to make sure the deserialization from Jackson is working
 * with the expected JSON files.  So there it is a bit more critical if those don't work as
 * the app itself could fail.
 */
data class SchoolCardClickedEvent(val schoolId: String)