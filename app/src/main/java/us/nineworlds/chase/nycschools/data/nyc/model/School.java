package us.nineworlds.chase.nycschools.data.nyc.model;

import androidx.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
final public class School {

    private final String dbn;
    private final String schoolName;
    private final String primaryAddress;
    private final String city;
    private final String state;
    private final String postalCode;

    public School(
            @JsonProperty("dbn") final String dbn,
            @JsonProperty("school_name") final String schoolName,
            @JsonProperty("primary_address_line_1") final String primaryAddress,
            @JsonProperty("city") final String city,
            @JsonProperty("state_code") final String state,
            @JsonProperty("zip") final String postalcode
    ) {
        this.dbn = dbn;
        this.schoolName = schoolName;
        this.primaryAddress = primaryAddress;
        this.city = city;
        this.state = state;
        this.postalCode = postalcode;
    }

    @Nullable
    public String getDbn() {
        return dbn;
    }

    @Nullable
    public String getSchoolName() {
        return schoolName;
    }

    @Nullable
    public String getPrimaryAddress() {
        return primaryAddress;
    }

    @Nullable
    public String getCity() {
        return city;
    }

    @Nullable
    public String getState() {
        return state;
    }

    @Nullable
    public String getPostalCode() {
        return postalCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        School school = (School) o;
        return Objects.equals(dbn, school.dbn) &&
                Objects.equals(schoolName, school.schoolName) &&
                Objects.equals(primaryAddress, school.primaryAddress) &&
                Objects.equals(city, school.city) &&
                Objects.equals(state, school.state) &&
                Objects.equals(postalCode, school.postalCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dbn, schoolName, primaryAddress, city, state, postalCode);
    }

    @Override
    public String toString() {
        return "School{" +
                "dbn='" + dbn + '\'' +
                ", schoolName='" + schoolName + '\'' +
                ", primaryAddress='" + primaryAddress + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }

}
