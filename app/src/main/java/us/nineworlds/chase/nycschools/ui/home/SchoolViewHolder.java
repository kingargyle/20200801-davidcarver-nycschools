package us.nineworlds.chase.nycschools.ui.home;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;

import us.nineworlds.chase.nycschools.data.events.SchoolCardClickedEvent;
import us.nineworlds.chase.nycschools.data.nyc.model.School;
import us.nineworlds.chase.nycschools.databinding.ItemSchoolBinding;

public class SchoolViewHolder extends RecyclerView.ViewHolder {

    private ItemSchoolBinding binding;

    @VisibleForTesting
    protected EventBus eventBus = EventBus.getDefault();

    public SchoolViewHolder(@NonNull View itemView) {
        super(itemView);
        binding = ItemSchoolBinding.bind(itemView);
    }

    public void bind(@NonNull School school) {
        binding.itemSchoolName.setText(school.getSchoolName());
        binding.itemSchoolAddress.setText(school.getPrimaryAddress());
        binding.itemSchoolCity.setText(school.getCity());
        binding.itemSchoolState.setText(school.getState());
        binding.itemSchoolPostalcode.setText(school.getPostalCode());
        binding.getRoot().setOnClickListener(v -> {
            SchoolCardClickedEvent event = new SchoolCardClickedEvent(school.getDbn());
            eventBus.post(event);
        });
    }
}
