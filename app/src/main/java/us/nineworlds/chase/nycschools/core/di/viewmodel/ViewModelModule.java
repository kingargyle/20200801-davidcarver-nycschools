package us.nineworlds.chase.nycschools.core.di.viewmodel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import us.nineworlds.chase.nycschools.ui.viewmodel.ViewModelDelegate;
import us.nineworlds.chase.nycschools.ui.viewmodel.ViewModelDelegateImpl;

@Module
@InstallIn(ApplicationComponent.class)
public class ViewModelModule {

    @Provides
    @Singleton
    ViewModelDelegate providesViewModelDelegate() {
        return new ViewModelDelegateImpl();
    }

}
