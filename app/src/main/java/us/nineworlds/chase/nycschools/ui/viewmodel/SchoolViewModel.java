package us.nineworlds.chase.nycschools.ui.viewmodel;

import androidx.annotation.NonNull;
import androidx.hilt.Assisted;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;

import com.uber.autodispose.AutoDispose;

import java.util.Collections;
import java.util.List;

import io.reactivex.Scheduler;
import timber.log.Timber;
import us.nineworlds.chase.nycschools.core.repository.SchoolRepository;
import us.nineworlds.chase.nycschools.data.nyc.model.School;
import us.nineworlds.chase.nycschools.data.nyc.model.SchoolDetail;

public class SchoolViewModel extends AutoDisposeViewModel {

    private SchoolRepository schoolRepository;
    private SavedStateHandle savedStateHandle;  // This could be used to help control when screens get reloaded.
    private Scheduler observeOnScheduler;

    private MutableLiveData<List<School>> allSchoolsLiveData = new MutableLiveData<>();
    private MutableLiveData<SchoolDetail> schoolDetail = new MutableLiveData<>();
    private MutableLiveData<String> errorMessage = new MutableLiveData<>();

    @ViewModelInject
    public SchoolViewModel(SchoolRepository schoolRepository,
                           Scheduler observeOnScheduler,
                           @Assisted SavedStateHandle savedStateHandle
    ) {
        this.schoolRepository = schoolRepository;
        this.observeOnScheduler = observeOnScheduler;
        this.savedStateHandle = savedStateHandle;
    }

    /**
     * Only non-mutable LiveData streams are returned to the view.
     * @return a non-mutable LiveData stream of a List of Schools
     */
    @NonNull
    public LiveData<List<School>> getAllSchoolsLiveData() {
        return allSchoolsLiveData;
    }

    /**
     * Only non-mutable LiveData streams are to the View.
     * @return a non-mutable LiveData stream of the SchoolDetail
     */
    @NonNull
    public LiveData<SchoolDetail> getSchoolDetail() {
        return schoolDetail;
    }

    public LiveData<String> getErrorMessage() {
        return errorMessage;
    }

    /**
     * Should be checking to see that we have a network connection before making these calls.
     *
     * Also need additional error handling so that a Error live data stream can be set and
     * observed.
     */
    public void retrieveListOfSchools() {
        schoolRepository.getSchools()
                .doOnError(error -> {
                    Timber.e(error, "Oops we got an error");

                    // We could also update a LiveData stream here with the error
                    // and display any type of error screen.  To be safe we'll send back
                    // an empty list
                })
                .onErrorReturn(error -> Collections.emptyList())
                .observeOn(observeOnScheduler)
                .as(AutoDispose.autoDisposable(this))
                .subscribe(schools -> allSchoolsLiveData.postValue(schools));
    }

    /**
     * Use this to look up a school id.
     * @param schoolId the school id to search for
     */
    public void lookupSchoolById(@NonNull String schoolId) {
        schoolRepository.getSchoolDetail(schoolId)
                .doOnError(error -> {
                    Timber.e(error, "Oops we got an error");

                    // We could also update a LiveData stream here with the error
                    // and display any type of error screen.
                })
                .onErrorReturn(error -> Collections.emptyList())
                .observeOn(observeOnScheduler)
                .as(AutoDispose.autoDisposable(this))
                .subscribe(schoolDetails -> {
                    if (!schoolDetails.isEmpty()) {
                        schoolDetail.postValue(schoolDetails.get(0));
                        errorMessage.postValue("");
                    } else {
                        errorMessage.postValue("No SAT information found.");
                    }
                });
    }
}
