package us.nineworlds.chase.nycschools.ui.viewmodel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModel;

/**
 * This class delegates the retrieval of the ViewModel and allows for easier testing of activities
 * or fragments that are using the view model. The point is to make it so that Mocks can be injected
 * during unit testing instead of the real view model, which should be tested separately.
 */
public interface ViewModelDelegate {

    /**
     * This class delegates the retrieval of the ViewModel and allows for easier testing of
     * activities.  How that is retrieved is up to the implementors.
     *
     * @param activity  The activity that should be used as the basis for retrieving the ViewModel
     * @param viewModel The class that represents the ViewModel to retrieve.  This should extend ViewModel
     * @param <A>       Generic that is the name of the Activity
     * @param <VM>      Generic that is the Name of the ViewModel
     * @return The ViewModel or null if it can't be found.
     */
    <A extends AppCompatActivity, VM extends ViewModel> VM findViewModel(A activity, Class<VM> viewModel);

    /**
     * This class delegates the retrieval of the ViewModel and allows for easier testing of
     * fragments with ViewModels.   How that is retrieved is up to the implementor
     *
     * @param fragment  The fragment to be used as the context for the ViewModel retrieval
     * @param viewModel The ViewModel to retrieve
     * @param <F>       Generic representing the Fragment
     * @param <VM>      Generic representing the ViewModel
     * @return the ViewModel or null if it can't be found.
     */
    <F extends FragmentActivity, VM extends ViewModel> VM findViewModel(F fragment, Class<VM> viewModel);

}
