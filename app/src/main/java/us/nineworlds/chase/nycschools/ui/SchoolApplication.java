package us.nineworlds.chase.nycschools.ui;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;
import timber.log.Timber;
import us.nineworlds.chase.nycschools.BuildConfig;

@HiltAndroidApp
public class SchoolApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
