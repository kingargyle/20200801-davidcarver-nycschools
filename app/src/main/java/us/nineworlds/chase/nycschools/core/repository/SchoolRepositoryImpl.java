package us.nineworlds.chase.nycschools.core.repository;

import androidx.annotation.NonNull;

import java.util.List;

import io.reactivex.Single;
import us.nineworlds.chase.nycschools.core.services.NYCDataSource;
import us.nineworlds.chase.nycschools.data.nyc.model.School;
import us.nineworlds.chase.nycschools.data.nyc.model.SchoolDetail;

public class SchoolRepositoryImpl implements SchoolRepository {

    private NYCDataSource dataSource;

    public SchoolRepositoryImpl(NYCDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    @NonNull
    public Single<List<School>> getSchools() {
        return dataSource.getAllSchools();
    }

    @Override
    @NonNull
    public Single<List<SchoolDetail>> getSchoolDetail(@NonNull String schoolId) {
        return dataSource.getSchoolDetail(schoolId);
    }
}
