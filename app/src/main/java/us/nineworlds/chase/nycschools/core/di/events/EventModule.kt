package us.nineworlds.chase.nycschools.core.di.events

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import org.greenrobot.eventbus.EventBus
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class EventModule {

    @Provides
    @Singleton
    fun providesEventBus() = EventBus.getDefault();

}