package us.nineworlds.chase.nycschools.data.nyc.model;

import androidx.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
final public class SchoolDetail {

    private final String dbn;
    private final String schoolName;
    private final int numberOfSatTestTakers;
    private final int averageReadingScore;
    private final int satMathAvgScore;
    private final int satWritingAvgScore;

    public SchoolDetail(
            @JsonProperty("dbn") final String dbn,
            @JsonProperty("school_name") final String schoolName,
            @JsonProperty("num_of_sat_test_takers") final int numberOfSatTestTakers,
            @JsonProperty("sat_critical_reading_avg_score") final int averageReadingScore,
            @JsonProperty("sat_math_avg_score") final int satMathAvgScore,
            @JsonProperty("sat_writing_avg_score") final int satWritingAvgScore
    ) {
        this.dbn = dbn;
        this.schoolName = schoolName;
        this.numberOfSatTestTakers = numberOfSatTestTakers;
        this.averageReadingScore = averageReadingScore;
        this.satMathAvgScore = satMathAvgScore;
        this.satWritingAvgScore = satWritingAvgScore;
    }

    @Nullable
    public String getDbn() {
        return dbn;
    }

    @Nullable
    public String getSchoolName() {
        return schoolName;
    }

    public int getNumberOfSatTestTakers() {
        return numberOfSatTestTakers;
    }

    public int getAverageReadingScore() {
        return averageReadingScore;
    }

    public int getSatMathAvgScore() {
        return satMathAvgScore;
    }

    public int getSatWritingAvgScore() {
        return satWritingAvgScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SchoolDetail that = (SchoolDetail) o;
        return numberOfSatTestTakers == that.numberOfSatTestTakers &&
                averageReadingScore == that.averageReadingScore &&
                satMathAvgScore == that.satMathAvgScore &&
                satWritingAvgScore == that.satWritingAvgScore &&
                Objects.equals(dbn, that.dbn) &&
                Objects.equals(schoolName, that.schoolName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dbn, schoolName, numberOfSatTestTakers, averageReadingScore, satMathAvgScore, satWritingAvgScore);
    }

    @Override
    public String toString() {
        return "SchoolDetail{" +
                "dbn='" + dbn + '\'' +
                ", schoolName='" + schoolName + '\'' +
                ", numberOfSatTestTakers=" + numberOfSatTestTakers +
                ", averageReadingScore=" + averageReadingScore +
                ", satMathAvgScore=" + satMathAvgScore +
                ", satWritingAvgScore=" + satWritingAvgScore +
                '}';
    }
}
