package us.nineworlds.chase.nycschools.core.di.network;

import androidx.annotation.NonNull;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import us.nineworlds.chase.nycschools.BuildConfig;
import us.nineworlds.chase.nycschools.core.services.NYCDataSource;

import static io.reactivex.schedulers.Schedulers.io;

@Module
@InstallIn(ApplicationComponent.class)
public class NetworkModule {

    private static final String NYC_X_APP_TOKEN = "X-App-Token";

    /**
     * Setup the requests so that the Application token is sent with every request to help
     * reduce the risk of being rate limited by the application
     * @return the configured OKHttpClient that adds the app token to every request
     */
    @Provides
    OkHttpClient providesOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        /*
         * While configuring this inline is convenient, it also makes
         * it harder to test.  Typically I'd extract this to its own class, and
         * either instantiate the object directly here, or we could setup a provides
         * and have it injected into the OkHttpClient instead.  It can then be tested
         * individually.
         */
        builder.addInterceptor(chain -> {
            Request request = chain.request();

            Request newRequest = request.newBuilder()
                    .addHeader(NYC_X_APP_TOKEN, BuildConfig.NYC_APP_TOKEN)
                    .build();

            return chain.proceed(newRequest);
        });

        return builder.build();
    }

    @Provides
    @Singleton
    @NonNull
    Retrofit providesRestClient(OkHttpClient client) {
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(BuildConfig.NYC_URL_ENDPOINT)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(io()))
                .build();
    }

    @Provides
    @Singleton
    @NonNull
    NYCDataSource providesNYCDataSource(@NonNull final Retrofit retrofit) {
        return retrofit.create(NYCDataSource.class);
    }

}
