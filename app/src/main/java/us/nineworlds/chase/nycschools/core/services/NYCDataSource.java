package us.nineworlds.chase.nycschools.core.services;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import us.nineworlds.chase.nycschools.data.nyc.model.School;
import us.nineworlds.chase.nycschools.data.nyc.model.SchoolDetail;

public interface NYCDataSource {

    @GET("resource/s3k6-pzi2.json")
    Single<List<School>> getAllSchools();

    @GET("resource/f9bf-2cp4.json")
    Single<List<SchoolDetail>> getSchoolDetail(@Query("dbn") final String dbn);

}
