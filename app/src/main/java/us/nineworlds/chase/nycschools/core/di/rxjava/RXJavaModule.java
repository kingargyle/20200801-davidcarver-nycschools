package us.nineworlds.chase.nycschools.core.di.rxjava;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

@Module
@InstallIn(ApplicationComponent.class)
public class RXJavaModule {

    /**
     * Note: If there would need to be more than one type of scheduler injected then this should
     * be a Qualified injection.  For our case and time we'll just inject the one scheduler and anotehr
     * during testing.
     *
     * @return
     */
    @Provides
    @Singleton
    Scheduler providesScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
