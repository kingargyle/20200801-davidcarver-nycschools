package us.nineworlds.chase.nycschools.ui.home;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;

import us.nineworlds.chase.nycschools.data.nyc.model.School;
import us.nineworlds.chase.nycschools.databinding.ItemSchoolBinding;

public class SchoolListAdapter extends ListAdapter<School, SchoolViewHolder> {

    public SchoolListAdapter() {
        super(new SchoolDiffUtil());
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemSchoolBinding binding = ItemSchoolBinding.inflate(layoutInflater, parent, false);
        return new SchoolViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder holder, int position) {
        holder.bind(getItem(position));
    }
}
