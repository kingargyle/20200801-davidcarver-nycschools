package us.nineworlds.chase.nycschools.core.di.repository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import us.nineworlds.chase.nycschools.core.repository.SchoolRepository;
import us.nineworlds.chase.nycschools.core.repository.SchoolRepositoryImpl;
import us.nineworlds.chase.nycschools.core.services.NYCDataSource;

@Module
@InstallIn(ApplicationComponent.class)
public class RepositoryModule {

    @Provides
    @Singleton
    SchoolRepository providesSchoolRepository(NYCDataSource nycDataSource) {
        return new SchoolRepositoryImpl(nycDataSource);
    }
}
