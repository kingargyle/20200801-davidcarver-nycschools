package us.nineworlds.chase.nycschools.ui.home;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import jp.wasabeef.recyclerview.animators.ScaleInAnimator;
import us.nineworlds.chase.nycschools.R;
import us.nineworlds.chase.nycschools.data.events.SchoolCardClickedEvent;
import us.nineworlds.chase.nycschools.data.nyc.model.School;
import us.nineworlds.chase.nycschools.databinding.ActivityMainBinding;
import us.nineworlds.chase.nycschools.ui.schooldetail.SchoolDetailsActivity;
import us.nineworlds.chase.nycschools.ui.viewmodel.SchoolViewModel;
import us.nineworlds.chase.nycschools.ui.viewmodel.ViewModelDelegate;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity {

    @Inject
    ViewModelDelegate viewModelDelegate;

    @Inject
    EventBus eventBus;

    private SchoolListAdapter adapter = new SchoolListAdapter();

    private SchoolViewModel schoolViewModel;
    private ActivityMainBinding binding;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        schoolViewModel = viewModelDelegate.findViewModel(this, SchoolViewModel.class);

        setupActionBar();
        setupRecyclerView();
        subscribe();

        schoolViewModel.retrieveListOfSchools();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    private void setupActionBar() {
        final MaterialToolbar materialToolbar = binding.toolbar;
        setSupportActionBar(materialToolbar);
        ActionBar supportActionBar = getSupportActionBar();

        if (supportActionBar != null) {
            supportActionBar.setDisplayShowHomeEnabled(true);
            supportActionBar.setLogo(R.mipmap.ic_launcher);
            supportActionBar.setDisplayUseLogoEnabled(true);
            supportActionBar.setTitle(" NYC Schools");
        }
    }

    private void setupRecyclerView() {
        RecyclerView recyclerView = binding.schoolListRecyclerview;
        // This assumes the layoutmanager has been defined in the layout file, if using kotlin I'd
        // probably use a binding or let? to check this otherwise I should really null check this
        // and add the layout manager if it isn't there.
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.setAdapter(adapter);
        DividerItemDecoration divider = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(divider);
        recyclerView.setItemAnimator(new ScaleInAnimator());
    }

    private void subscribe() {
        LiveData<List<School>> schoolsLiveData = schoolViewModel.getAllSchoolsLiveData();
        schoolsLiveData.observe(this, this::submitDataToAdapter);
    }

    private void submitDataToAdapter(@NonNull List<School> schoolList) {
        if (adapter != null) {
            adapter.submitList(schoolList);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subscribeToSchoolCardClickedEvent(@NonNull SchoolCardClickedEvent event) {
        Intent intent = new Intent(this, SchoolDetailsActivity.class);
        intent.putExtra("nyc_school_id", event.getSchoolId());

        startActivity(intent);
    }
}